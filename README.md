API Testing using Python PyTest Framework

- This test will verify the three acceptance criteria of the given endpoint
- The pytest framwork helps to add additional checks with ease

Requirements
Install Python3 and above with the following commands

pip install -U pytest
pip install requests
pip install jsonpath

To run the test use the below command

python3 .\test_sandbox_api.py
