import requests

ENDPOINT = "https://api.tmsandbox.co.nz/v1/Categories/6327/Details.json?catalogue=false"
response = requests.get(ENDPOINT)
data = response.json()

# Verify if the endpoint return code is successful
def test_call_endpoint():
    assert response.status_code == 200
    pass
# Verify the first two acceptance criteria
def test_get_Name():
    assert data['Name'] == "Carbon credits" # Name is returned
    assert data["CanRelist"] == True # Relisting is True


def test_get_promotions():
    for promo in data['Promotions']:
        if promo['Name'] == "Gallery":
            assert promo['Description'] == "Good position in category" # Promo Gallery is retuning the right value


